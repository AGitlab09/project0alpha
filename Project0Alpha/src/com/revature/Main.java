package com.revature;

import java.util.Scanner;


public class Main 
{
	public static void main(String[] args)
	{
		// main stuff
		
		// Welcome message
		System.out.println("Welcome to the Bank!");
		
		// Main menu options
		System.out.println("\nWhat would you like to do?");
		//System.out.println("(Enter the number of your selection.)");
		System.out.println(" 1. Login");
		System.out.println(" 2. Create an Account");
		
		// User input.. 
		System.out.print("\nEnter selection number: ");
		Scanner mainMenuChoice = new Scanner(System.in);
		
		//System.out.print("User choice: ");
		//System.out.println(mainMenuChoice.nextLine());
		
		// Output user choice
		String userChoice = mainMenuChoice.nextLine();
		System.out.print("User choice: ");
		System.out.println(userChoice);
		
		
		//String userChoice;  // declare variable for user selection..
		/*
		do
		{
			// User input.. 
			System.out.print("\nEnter selection number: ");
			Scanner mainMenuChoice = new Scanner(System.in);
			
			//System.out.print("User choice: ");
			//System.out.println(mainMenuChoice.nextLine());
			
			userChoice = mainMenuChoice.nextLine();
			System.out.print("User choice: ");
			System.out.println(userChoice);
		}while(userChoice.equalsIgnoreCase("1")==false && userChoice.equalsIgnoreCase("2")==false);
		*/
		
		// Repeat userChoice prompt while invalid choice..
		while(userChoice.equalsIgnoreCase("1")==false && userChoice.equalsIgnoreCase("2")==false)
		{
			System.out.print("\nEnter selection number (1.Login, 2.Create Account): ");
			mainMenuChoice = new Scanner(System.in);
			
			//System.out.print("User choice: ");
			//System.out.println(mainMenuChoice.nextLine());
			
			userChoice = mainMenuChoice.nextLine();
			System.out.print("User choice: ");
			System.out.println(userChoice);
		}
		
		// output 'Thank you' when valid user choice input
		System.out.println("\nThank you.");
		
	}
}





